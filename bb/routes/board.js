const express = require('express');
const reply_routes = require('./add_reply')

function routes(db) {
  let router = express.Router();

  router.get('/', function(req, res, next) {
    db('threads').then(rows => {
      res.render('index', { threads: rows });
    })
  });

  router.get('/add_thread', function(req, res, next) {
    res.render('add_thread');
  });

  router.post('/add_thread', function(req, res, next) {
    // FIXME: xss probably, sanitize
    db('threads').insert({
      subject: req.body.subject,
      message: req.body.message,
      added_on: db.fn.now()
    })
    .then((id) => {
      res.redirect("/thread/"+id)
    });
  });

  router.get('/thread/:id', function(req, res, next) {
    // FIXME: display 404 if thread doesn't exist
    Promise.all([
      db("threads").where({id: req.params.id}).first().then(
        (thread) => {
          if (thread == undefined) {
            return Promise.reject({
              message: "Thread doesn't exist!"
            })
          } else {
            return thread
          }
        }
      ),
      db("replies").where({reply_to: req.params.id})
    ]).then(([thread, replies]) => {
      res.render('display_thread', { thread: thread, replies: replies });
    }).catch(next)
  });

  router.use("/thread/", reply_routes(db));
  /*router.get('/thread/:id/add_reply', function(req, res, next) {
    // FIXME: display 404 if thread doesn't exist
    res.render('add_reply');
  });

  router.post('/thread/:id/add_reply', function(req, res, next) {
    // FIXME: xss probably, sanitize
    db('replies').insert({
      message: req.body.message,
      reply_to: req.params.id,
      added_on: db.fn.now()
    }).then(() => {
      res.render('thread_added', { 
        message: req.body.message,
      });
    });
  });*/


  return router
}

module.exports = {routes: routes};
