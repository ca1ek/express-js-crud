const express = require('express');

function add_reply_routes(db) {
  let router = express.Router();
  
  router.get('/:id/add_reply', function(req, res, next) {
    // FIXME: display 404 if thread doesn't exist
    res.render('add_reply');
  });

  router.post('/:id/add_reply', function(req, res, next) {
    // FIXME: xss probably, sanitize
    db('replies').insert({
      message: req.body.message,
      reply_to: req.params.id,
      added_on: db.fn.now()
    }).then(() => {
      res.redirect("/thread/"+req.params.id)
    });
  });

  return router;
}

module.exports = add_reply_routes;