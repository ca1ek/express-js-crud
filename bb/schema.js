var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: "./mydb.sqlite"
    },
    useNullAsDefault: true
});  

function init(db) {
    Promise.all([
        db.schema.createTable("threads", (table) => {
            table.increments();
            table.string("subject");
            table.string("message");
            table.timestamp("added_on");
        }).then(() => {
            console.log("threads table added");
        }),

        db.schema.createTable("replies", (table) => {
            table.increments();
            table.string("message");
            table.integer("reply_to").unsigned();
            table.foreign("reply_to").references("threads.id")
            table.timestamp("added_on");
        }).then(() => {
            console.log("replies table added");
        }),

        db.schema.createTable("accounts", (table) => {
            table.increments();
            table.string("username");
            table.string("password");
            table.timestamp("registered_on");
        }).then(() => {
            console.log("accounts table added");
        }),

        db.schema.createTable("tokens", (table) => {
            table.increments();
            table.integer("account").unsigned();
            table.foreign("account").references("accounts.id");
            table.string("token");
            table.timestamp("created_on");
        }).then(() => {
            console.log("tokens table added");
        }),
    ]).then(() => {
        console.log("done.");
        db.destroy();
    })
}

init(knex)